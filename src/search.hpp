#include <string>
using namespace std;

int search (string text, string pattern)
{
	for (unsigned int i = 0; i < text.size(); i++)
	{
		for (unsigned int n = 0; n < pattern.size(); n++)
		{
			if (text[i+n] == pattern[n])
			{
				if (n == pattern.size()-1)
				{
					return i;
				}
			}	
			else
			{
				break;
			}
		}
	}
	return -1;
}
